package internal

import (
	"os"
	"strings"
)

var hostname string

func init() {
	fqdn, err := os.Hostname()
	if err != nil {
		panic(err)
	}

	hostname = strings.Split(fqdn, ".")[0]
}

func GetHostname() string {
	return hostname
}
