package main

import (
	"flag"
	"log"
	"time"

	"gitlab.com/mergetb/exptools/forensics/measurement/internal"
	"gitlab.com/mergetb/exptools/forensics/measurement/pkg/config"
	"gitlab.com/mergetb/exptools/forensics/measurement/pkg/influx"
	"gitlab.com/mergetb/exptools/forensics/measurement/pkg/sampler"
)

const (
	defaultConf string = "/etc/mergetb/forensics.yml"
)

var (
	conf   string
	tags   map[string]string
	period int = 1
)

func main() {
	flag.StringVar(&conf, "config", defaultConf, "path to configuration file")
	flag.Parse()

	cfg, err := config.GetConfigFromYaml(conf)
	if err != nil {
		log.Fatal(err)
	}

	if cfg.Period != 0 {
		period = cfg.Period
	}

	tags = cfg.Tags

	if cfg.InfluxServer == "" {
		panic("config must provide influx_server")
	}

	if cfg.InfluxDatabase == "" {
		panic("config must provide influx_database")
	}

	// create channel to write to influx
	db, err := influx.NewDatabase(cfg.InfluxServer, cfg.InfluxDatabase)
	if err != nil {
		panic(err)
	}

	log.Printf("writing data to influxdb at server:%s database:%s", cfg.InfluxServer, cfg.InfluxDatabase)

	// start channel writer
	go db.WriteChannel()

	samplers := getSamplers(cfg)
	startSamplers(db, samplers)
}

func getSamplers(cfg *config.Config) []sampler.Sampler {
	var s []sampler.Sampler

	if cfg.IpStat {
		s = append(s, new(sampler.IpStatSampler))
	}

	return s
}

func startSamplers(db *influx.InfluxDatabase, samplers []sampler.Sampler) {

	for _, s := range samplers {
		err := s.Start()
		if err != nil {
			log.Fatal(err)
		}
	}

	log.Printf("sampling interval=%d sec", period)

	for {
		time.Sleep(time.Second * time.Duration(period))

		ts := time.Now()

		for _, s := range samplers {
			points, err := s.Sample()
			if err != nil {
				log.Fatal(err)
			}

			for _, p := range points {
				p.Timestamp = ts

				// ensure hostname is a tag and add custom config tag
				if _, ok := p.Tags["hostname"]; !ok {
					p.Tags["hostname"] = internal.GetHostname()
				}

				for k, v := range tags {
					p.Tags[k] = v
				}

				db.Channel <- p
			}
		}
	}

	for _, s := range samplers {
		err := s.Stop()
		if err != nil {
			log.Fatal(err)
		}
	}
}
