package types

// ip stat -s -s
type IpStat64Dir struct {
	Bytes        int `json:"bytes"`
	Packets      int `json:"packets"`
	Errors       int `json:"errors"`
	Dropped      int `json:"dropped"`
	OverErrors   int `json:"over_errors"`
	Multicast    int `json:"multicast"`
	LengthErrors int `json:"length_errors"`
	CrcErrors    int `json:"crc_errors"`
	FrameErrors  int `json:"frame_errors"`
	FifoErrors   int `json:"fifo_errors"`
	MissedErrors int `json:"missed_errors"`
}

type IpStat64 struct {
	Rx IpStat64Dir `json:"rx"`
	Tx IpStat64Dir `json:"tx"`
}

type IpStatIfx struct {
	Ifindex   int      `json:"ifindex"`
	Ifname    string   `json:"ifname"`
	Flags     []string `json:"flags"`
	Mtu       int      `json:"mtu"`
	Qdisc     string   `json:"qdisc"`
	Operstate string   `json:"operstate"`
	Linkmode  string   `json:"linkmode"`
	Group     string   `json:"group"`
	Txqlen    int      `json:"txqlen"`
	LinkType  string   `json:"link_type"`
	Address   string   `json:"address"`
	Broadcast string   `json:"broadcast"`
	Stats     IpStat64 `json:"stats64"`
}

type IpStat []IpStatIfx
