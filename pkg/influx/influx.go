package influx

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"strings"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"

	"gitlab.com/mergetb/exptools/forensics/measurement/pkg/sampler"
)

type InfluxDatabase struct {
	server  string
	name    string
	Channel chan *sampler.Point
}

func NewDatabase(server, name string) (*InfluxDatabase, error) {
	ch := make(chan *sampler.Point)

	db := &InfluxDatabase{
		server:  server,
		name:    name,
		Channel: ch,
	}

	err := db.createDatabase()
	if err != nil {
		return nil, err
	}

	return db, nil
}

func (d *InfluxDatabase) dbQuery(query string) error {
	db_url := d.server + "/query"
	data := url.Values{}
	data.Set("q", fmt.Sprintf("%s DATABASE %s", query, d.name))

	client := &http.Client{}
	r, err := http.NewRequest("POST", db_url, strings.NewReader(data.Encode()))
	if err != nil {
		return err
	}

	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	res, err := client.Do(r)
	if err != nil {
		return err
	}

	if res.StatusCode != 200 {
		return fmt.Errorf("could not %s database: status code %d", query, res.StatusCode)
	}

	res.Body.Close()

	return nil

}

func (d *InfluxDatabase) createDatabase() error {
	return d.dbQuery("CREATE")
}

func (d *InfluxDatabase) WriteChannel() {
	err := d.createDatabase()
	if err != nil {
		panic(err)
	}

	client := influxdb2.NewClient(d.server, "")
	defer client.Close()

	writeAPI := client.WriteAPIBlocking("", d.name)

	for p := range d.Channel {
		pt := influxdb2.NewPoint(
			p.Name,
			p.Tags,
			p.Fields,
			p.Timestamp,
		)

		err := writeAPI.WritePoint(context.Background(), pt)
		if err != nil {
			panic(err)
		}
	}

}
