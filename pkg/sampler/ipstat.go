package sampler

import (
	"encoding/json"
	"os/exec"

	"gitlab.com/mergetb/exptools/forensics/measurement/pkg/types"
)

type IpStatSampler struct {
	// maps interface name to interface stats
	prev map[string]types.IpStatIfx
}

func (n *IpStatSampler) Start() error {
	// collect the first sample so that the first measured sample has adjusted data
	_, err := n.Sample()
	return err
}

func (n *IpStatSampler) Stop() error {
	return nil
}

func (n *IpStatSampler) Sample() ([]*Point, error) {
	cmd := exec.Command("ip", "-s", "-s", "-j", "link", "show")

	out, err := cmd.Output()
	if err != nil {
		return nil, err
	}

	is := types.IpStat{}
	err = json.Unmarshal(out, &is)
	if err != nil {
		return nil, err
	}

	all := types.IpStatIfx{
		Ifname: "all",
	}

	p := []*Point{}

	cur := make(map[string]types.IpStatIfx)

	// one point for each ifx
	for _, i := range is {
		cur[i.Ifname] = i

		// subtract valus from previous sample
		n.subtractPrev(&i)

		pt, err := n.toPoint(&i, &all)
		if err != nil {
			return nil, err
		}
		p = append(p, pt)
	}

	// convert 'all' to point
	pt, err := n.toPoint(&all, nil)
	if err != nil {
		return nil, err
	}
	p = append(p, pt)

	// stash away prev
	n.prev = cur

	return p, nil
}

func (n *IpStatSampler) subtractPrev(i *types.IpStatIfx) {
	if n.prev == nil {
		return
	}

	if pifx, ok := n.prev[i.Ifname]; ok {
		rx := &i.Stats.Rx
		tx := &i.Stats.Tx
		prx := &pifx.Stats.Rx
		ptx := &pifx.Stats.Tx

		rx.Bytes -= prx.Bytes
		rx.Packets -= prx.Packets
		rx.Errors -= prx.Errors
		rx.Dropped -= prx.Dropped
		tx.Bytes -= ptx.Bytes
		tx.Packets -= ptx.Packets
		tx.Errors -= ptx.Errors
		tx.Dropped -= ptx.Dropped
	}
}

// flatten the data out into a single dimension and format into a generic Point
// structure
func (n *IpStatSampler) toPoint(i, all *types.IpStatIfx) (*Point, error) {
	rx := &i.Stats.Rx
	tx := &i.Stats.Tx

	p := &Point{
		Name: "ipstat",
		Tags: map[string]string{
			"ifname": i.Ifname,
		},
		Fields: map[string]interface{}{
			"rx_bytes":   rx.Bytes,
			"rx_packets": rx.Packets,
			"rx_errors":  rx.Errors,
			"rx_dropped": rx.Dropped,
			"tx_bytes":   tx.Bytes,
			"tx_packets": tx.Packets,
			"tx_errors":  tx.Errors,
			"tx_dropped": tx.Dropped,
		},
	}

	if all != nil {
		drx := &all.Stats.Rx
		dtx := &all.Stats.Tx

		drx.Bytes += rx.Bytes
		drx.Packets += rx.Packets
		drx.Errors += rx.Errors
		drx.Dropped += rx.Dropped
		dtx.Bytes += tx.Bytes
		dtx.Packets += tx.Packets
		dtx.Errors += tx.Errors
		dtx.Dropped += tx.Dropped
	}

	return p, nil
}
