package sampler

import "time"

type Point struct {
	Name      string
	Tags      map[string]string
	Fields    map[string]interface{}
	Timestamp time.Time
}

type Sampler interface {
	Start() error
	Sample() ([]*Point, error)
	Stop() error
}
