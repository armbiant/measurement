all: build/sampler

.PHONY: clean
clean:
	rm -rf build

build/sampler: cmd/sampler/*.go pkg/config/*.go pkg/influx/*.go pkg/sampler/*.go pkg/types/*.go internal/*.go
	CGO_ENABLED=0 go build -o $@ cmd/sampler/*.go
